﻿using NetCoreSwaggerUI.Entities;
using System.Collections.Generic;

namespace NetCoreSwaggerUI.Business.Abstract
{
    public interface IPersonService
    {
        List<Person> GetAll();
        Person GetById(int id);
        void Create(Person entity);
        void Update(Person entity);
        void Delete(Person entity);
    }
}
