﻿using NetCoreSwaggerUI.Entities;

namespace NetCoreSwaggerUI.DataAccess.Abstract
{
    public interface IPersonDal : IRepositoryDal<Person>
    {

    }
}
