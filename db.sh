#!/bin/bash
now=$(date)

echo $now

touch db.sql
db="CREATE DATABASE IF NOT EXISTS SwaggerDB;"
usus="use test;"
tbl="CREATE TABLE IF NOT EXISTS Persons (
   id int NOT NULL AUTO_INCREMENT,
  adi varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  soyadi varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  telefon varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  date_time varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;"
ins="INSERT INTO Persons (adi, soyadi, telefon, date_time) VALUES('Deniz', 'TURKMEN', '4567848665', '07072021');"

echo "Sql Oluşturuluyor....."
echo $db >> db.sql
echo $usus >> db.sql
echo $tbl >> db.sql
echo $ins >> db.sql
echo use test; >> db.sql


docker exec -i deniz-mysql mysql -uroot -pexample < db.sql

#rm -rf *.sql

echo "oluştu...."
