FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build-env
WORKDIR /app

# RUN apt-get update
# RUN apt-get install -y wget
# RUN wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
# RUN dpkg -i packages-microsoft-prod.deb
# RUN apt-get update; apt-get install -y apt-transport-https && apt-get update && apt-get install -y dotnet-sdk-3.1

# RUN dotnet tool install --global dotnet-ef

RUN apt-get update
RUN apt-get install -y vim

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . .
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "NetCoreSwaggerUI.dll"]
